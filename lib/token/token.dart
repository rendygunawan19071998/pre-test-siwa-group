import 'package:movies/helper/core.dart';

class MOVIESTOKEN {
  Future<void> saveMovies(List<MoviesModel> movies) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('movies', jsonEncode(movies));
    // print("Save movies : $movies");
  }

  // ignore: missing_return
  Future<String> deleteMovies() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('movies');
  }
}
