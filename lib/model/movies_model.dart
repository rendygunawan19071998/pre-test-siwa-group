// To parse this JSON data, do
//
//     final moviesModel = moviesModelFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

List<MoviesModel> moviesModelFromJson(String str) => List<MoviesModel>.from(
    json.decode(str).map((x) => MoviesModel.fromJson(x)));

String moviesModelToJson(List<MoviesModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MoviesModel {
  MoviesModel({
    this.id,
    this.title,
    this.director,
    this.summary,
    this.genres,
  });

  int id;
  String title;
  String director;
  String summary;
  String genres;

  factory MoviesModel.fromJson(Map<String, dynamic> json) => MoviesModel(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        director: json["director"] == null ? null : json["director"],
        summary: json["summary"] == null ? null : json["summary"],
        genres: json["genres"] == null ? null : json["genres"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "director": director == null ? null : director,
        "summary": summary == null ? null : summary,
        "genres": genres == null ? null : genres,
      };
}
