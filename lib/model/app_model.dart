import 'package:movies/helper/core.dart';

abstract class AppModel extends ChangeNotifier {
  Future readJson();

  List get master;
  List get dataMovie;
}

class AppModelImplementation extends AppModel {
  List<MoviesModel> lpMaster = [];
  List dataMovies = [];

  AppModelImplementation() {
    /// lets pretend we have to do some async initialization
    Future.delayed(Duration(seconds: 3)).then((_) => getIt.signalReady(this));
  }

  @override
  List get master => lpMaster;
  List get dataMovie => dataMovie;

  @override
  Future<MoviesModel> readJson() async {
    final prefs = await SharedPreferences.getInstance();
    final List<dynamic> jsonData =
        jsonDecode(prefs.getString('movies') ?? '[]');
    print('jsonData :: $jsonData');

    lpMaster = jsonData.map((data) => MoviesModel.fromJson(data)).toList();
    dataMovies = jsonData;

    // notifyListeners();
  }
}
