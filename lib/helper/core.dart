export 'dart:async';
export 'dart:convert';
export 'package:flutter/material.dart';
export 'package:movies/model/movies_model.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:movies/model/app_model.dart';
export 'package:auto_route/auto_route.dart';
export 'package:get_it/get_it.dart';

export 'package:movies/helper/widget.dart';
export 'package:movies/model/movies_model.dart';
export 'package:movies/token/token.dart';

// pages
export 'package:movies/pages/home.dart';
export 'package:movies/pages/movie.dart';
export 'package:movies/main.dart';
