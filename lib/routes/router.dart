import 'package:auto_route/auto_route_annotations.dart';
import 'package:movies/helper/core.dart';

@AdaptiveAutoRouter(routes: <AutoRoute>[
  AutoRoute(page: HomePage, initial: true),
  AutoRoute(page: MoviePage),
])
class $FlutterRouter {}
