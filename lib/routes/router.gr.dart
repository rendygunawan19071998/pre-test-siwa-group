// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';

import '../helper/core.dart';

class Routes {
  static const String homePage = '/';
  static const String moviePage = '/movie-page';
  static const all = <String>{
    homePage,
    moviePage,
  };
}

class FlutterRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.homePage, page: HomePage),
    RouteDef(Routes.moviePage, page: MoviePage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    HomePage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => const HomePage(),
        settings: data,
      );
    },
    MoviePage: (data) {
      final args = data.getArgs<MoviePageArguments>(
        orElse: () => MoviePageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MoviePage(
          key: args.key,
          id: args.id,
          title: args.title,
          director: args.director,
          summary: args.summary,
          genres: args.genres,
          status: args.status,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// MoviePage arguments holder class
class MoviePageArguments {
  final Key key;
  final int id;
  final String title;
  final String director;
  final String summary;
  final String genres;
  final bool status;
  MoviePageArguments({
    this.key,
    this.id,
    this.title,
    this.director,
    this.summary,
    this.genres,
    this.status,
  });
}
