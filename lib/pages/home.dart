import 'package:auto_route/auto_route.dart';
import 'package:mobx/mobx.dart';
import 'package:movies/helper/core.dart';
import 'package:movies/routes/router.gr.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
// ignore: prefer_final_fields
  @observable
  TextEditingController _searchTxt = TextEditingController();
  String searchString = "";
  List<MoviesModel> lpMaster = [];
  List<MoviesModel> lp1 = [];
  List<MoviesModel> dataMovies = [];

  Future<MoviesModel> readJson() async {
    // await MOVIESTOKEN().deleteMovies();
    final prefs = await SharedPreferences.getInstance();
    final List<dynamic> jsonData =
        jsonDecode(prefs.getString('movies') ?? '[]');
    print('jsonData :: $jsonData');

    setState(() {
      lpMaster = jsonData.map<MoviesModel>((jsonItem) {
        return MoviesModel.fromJson(jsonItem);
      }).toList();
      dataMovies = lpMaster;
    });
  }

  @override
  void initState() {
    getIt
        .isReady<AppModel>()
        .then((_) => getIt<AppModel>().addListener(update));
    readJson();
    super.initState();
  }

  @action
  void update() => readJson();

  @override
  Widget build(BuildContext context) {
    var sizes = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Movies Collection'),
        ),
      ),
      body: Container(
        height: sizes.height,
        width: sizes.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _searchbar(),
            hbox(15),
            dataMovies.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                    itemCount: dataMovies.length,
                    itemBuilder: (BuildContext context, int index) {
                      var datas = dataMovies[index];
                      // print('datas :: ${datas.genres}');
                      return Padding(
                        padding: EdgeInsets.fromLTRB(10, 8, 10, 0),
                        child: InkWell(
                          onTap: () {
                            ExtendedNavigator.root.push(
                              Routes.moviePage,
                              arguments: MoviePageArguments(
                                id: datas.id,
                                director: datas.director,
                                genres: datas.genres,
                                summary: datas.summary,
                                title: datas.title,
                                status: true,
                              ),
                            );
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  color: Colors.black,
                                  width: 1,
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(14),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _textList(
                                      // lpMaster[index].title,
                                      datas.title,
                                      Colors.black,
                                      FontWeight.bold,
                                      18,
                                    ),
                                    _textList(
                                      datas.director,
                                      // lpMaster[index].director,
                                      Colors.black,
                                      FontWeight.w500,
                                      16,
                                    ),
                                    hbox(10),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        _textList(
                                          datas.genres,
                                          // lpMaster[index].genres,
                                          Colors.black,
                                          FontWeight.w500,
                                          14,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )),
                        ),
                      );
                    },
                  ))
                // ignore: avoid_unnecessary_containers
                : Container(
                    child: Center(
                      child: Text(
                        ('Movies Data is empty...'),
                      ),
                    ),
                  ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ExtendedNavigator.root.push(
            Routes.moviePage,
          );
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Widget _textList(String text, Color warna, FontWeight fw, double fs) {
    return Text(
      text,
      style: TextStyle(
        color: warna,
        fontSize: fs,
        fontWeight: fw,
      ),
    );
  }

  _searchbar() {
    var sizes = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 8, 10, 0),
      child: Container(
        width: sizes.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 1),
        ),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: TextFormField(
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 15, right: 10),
              border: InputBorder.none,
              // suffixIcon: IconButton(
              //   onPressed: () async {
              //     setState(() {
              //       _searchTxt.clear();
              //       lp1 = [];
              //       lp1 = lpMaster;
              //     });
              //   },
              //   icon: Icon(Icons.close),
              // ),
              labelText: "Search by title...",
              labelStyle: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
              ),
            ),
            controller: _searchTxt,
            onChanged: (value) {
              searchString = value;
              List<MoviesModel> result = [];
              result = lpMaster
                  .where((element) => element.title
                      .toLowerCase()
                      .contains(searchString.toLowerCase()))
                  .toList();
              setState(() {
                dataMovies = result;
              });
            },
          ),
        ),
      ),
    );
  }
}
