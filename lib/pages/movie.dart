// ignore_for_file: avoid_unnecessary_containers, prefer_final_fields

import 'package:mobx/mobx.dart';
import 'package:movies/helper/core.dart';
import 'package:movies/routes/router.gr.dart';

class MoviePage extends StatefulWidget {
  final int id;
  final String title;
  final String director;
  final String summary;
  final String genres;
  final bool status;
  const MoviePage(
      {Key key,
      this.id,
      this.title,
      this.director,
      this.summary,
      this.genres,
      this.status})
      : super(key: key);

  @override
  _MoviePageState createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  @observable
  final _formKey = GlobalKey<FormState>();
  TextEditingController title = TextEditingController();
  TextEditingController director = TextEditingController();
  TextEditingController summary = TextEditingController();
  int id = 0;
  int tempId = 0;
  List<String> _selected = [];
  List<MoviesModel> _list = [];
  List<MoviesModel> _list2 = [];

  bool pAction = false;
  bool pAnimation = false;
  bool pDrama = false;
  bool pScifi = false;
  bool edit = false;

  Future<MoviesModel> readJson() async {
    final prefs = await SharedPreferences.getInstance();
    final List<dynamic> jsonData =
        jsonDecode(prefs.getString('movies') ?? '[]');
    print('jsonData :: $jsonData');

    setState(() {
      _list = jsonData.map((data) => MoviesModel.fromJson(data)).toList();
      for (var i = 0; i < _list.length; i++) {
        int temp = _list[i].id;
        // print("temp id :: $temp");
        tempId = temp;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
    title.text = widget.title ?? '';
    id = widget.id;
    director.text = widget.director ?? '';
    summary.text = widget.summary ?? '';
    edit = widget.status ?? false;
  }

  @override
  Widget build(BuildContext context) {
    var sizes = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            onTap: () async {
              print('Pressed delete');
              _list.removeWhere((item) => item.id == id);
              // print('list after delete :: ${json.encode(_list)}');
              await MOVIESTOKEN().saveMovies(_list);
              ExtendedNavigator.root.push(
                Routes.homePage,
              );
            },
            child: Container(
              child: Center(
                child: Icon(Icons.delete),
              ),
            ),
          ),
          wbox(10),
          InkWell(
            onTap: () {
              edit == false ? _saveMovie() : _editMovie();
            },
            child: Container(
              child: Center(
                child: Icon(Icons.save),
              ),
            ),
          ),
          wbox(10),
        ],
      ),
      body: Container(
        height: sizes.height,
        width: sizes.width,
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                formField(title, 'Title'),
                formField(director, 'Director'),
                ffSummary(summary, 'Summary'),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            pAction = !pAction;
                            print('status action :: $pAction');
                          });

                          if (pAction == true) {
                            if (!_selected.contains('Action')) {
                              print('tidak punya Action');
                              _selected.add('Action');
                            }
                          } else {
                            _selected.remove('Action');
                          }
                        },
                        child: Container(
                          child: Chip(
                            avatar: pAction
                                ? CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.black,
                                    ),
                                  )
                                : null,
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelPadding: EdgeInsets.all(5.0),
                            label: Text(
                              'Action',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            backgroundColor: Colors.red,
                          ),
                        ),
                      ),
                      wbox(20),
                      InkWell(
                        onTap: () {
                          setState(() {
                            pAnimation = !pAnimation;
                            print('status Animation :: $pAnimation');
                          });

                          if (pAnimation == true) {
                            if (!_selected.contains('Animation')) {
                              print('tidak punya Animation');
                              _selected.add('Animation');
                            }
                          } else {
                            _selected.remove('Animation');
                          }
                        },
                        child: Container(
                          child: Chip(
                            avatar: pAnimation
                                ? CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.black,
                                    ),
                                  )
                                : null,
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelPadding: EdgeInsets.all(5.0),
                            label: Text(
                              'Animation',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            backgroundColor: Colors.blue,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            pDrama = !pDrama;
                            print('status Drama :: $pDrama');
                          });

                          if (pDrama == true) {
                            if (!_selected.contains('Drama')) {
                              print('tidak punya Drama');
                              _selected.add('Drama');
                            }
                          } else {
                            _selected.remove('Drama');
                          }
                        },
                        child: Container(
                          child: Chip(
                            avatar: pDrama
                                ? CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.black,
                                    ),
                                  )
                                : null,
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelPadding: EdgeInsets.all(5.0),
                            label: Text(
                              'Drama',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            backgroundColor: Colors.yellow[900],
                          ),
                        ),
                      ),
                      wbox(20),
                      InkWell(
                        onTap: () {
                          setState(() {
                            pScifi = !pScifi;
                            print('status Sci-Fi :: $pScifi');
                          });

                          if (pScifi == true) {
                            if (!_selected.contains('Sci-Fi')) {
                              print('tidak punya Sci-Fi');
                              _selected.add('Sci-Fi');
                            }
                          } else {
                            _selected.remove('Sci-Fi');
                          }
                        },
                        child: Container(
                          child: Chip(
                            avatar: pScifi
                                ? CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.black,
                                    ),
                                  )
                                : null,
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            labelPadding: EdgeInsets.all(5.0),
                            label: Text(
                              'Sci-Fi',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            backgroundColor: Colors.orange[900],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _editMovie() async {
    print('you press edit');
    if (_formKey.currentState.validate()) {
      if (_selected.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Genre belum dipilih')),
        );
      } else {
        print('validated');
        print('title :: ${title.text} ');
        print('director :: ${director.text} ');
        print('summary :: ${summary.text} ');

        List gen = _selected;
        // print('gen :: $gen');
        String temp = gen.toString();
        // print('temp :: $temp');
        temp = temp.replaceAll("[", "");
        temp = temp.replaceAll("]", "");
        temp = temp.replaceAll(",", " /");
        print('genres :: $temp');

        int index = _list.indexWhere((element) => element.id == id);
        print('index list :: $index');
        var data = MoviesModel(
            id: id,
            title: title.text,
            director: director.text,
            summary: summary.text,
            genres: temp);

        _list.isNotEmpty ? _list.removeWhere((item) => item.id == id) : null;
        _list.insert(index, data);

        // print('data :: ${json.encode(data)}');
        // _list.add(data);
        await MOVIESTOKEN().saveMovies(_list);
        ExtendedNavigator.root.push(
          Routes.homePage,
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Periksa data anda, masih ada data yang kosong')),
      );
    }
  }

  Future<void> _saveMovie() async {
    print('Pressed save');
    if (_formKey.currentState.validate()) {
      if (_selected.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Genre belum dipilih')),
        );
      } else {
        print('validated');
        print('title :: ${title.text} ');
        print('director :: ${director.text} ');
        print('summary :: ${summary.text} ');
        // print('chip :: $_selected');

        List gen = _selected;
        // print('gen :: $gen');
        String temp = gen.toString();
        // print('temp :: $temp');
        temp = temp.replaceAll("[", "");
        temp = temp.replaceAll("]", "");
        temp = temp.replaceAll(",", " /");
        print('genres :: $temp');

        print('id :: $id');

        if (id == null) {
          id = tempId;
          id++;
          // print('id diisi temp :: $id');
        } else {
          id = id;
        }
        var data = MoviesModel(
            id: id,
            title: title.text,
            director: director.text,
            summary: summary.text,
            genres: temp);

        print('data :: $data');
        // var encode = json.encode(data);
        // print('encode :: $encode');
        _list.add(data);
        await MOVIESTOKEN().saveMovies(_list);
        ExtendedNavigator.root.push(
          Routes.homePage,
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Periksa data anda, masih ada data yang kosong')),
      );
    }
  }

  Widget formField(TextEditingController cont, String text) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 1),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 10, right: 10),
          child: TextFormField(
            controller: cont,
            onSaved: (value) {
              cont.text = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Data kosong';
              }
              return null;
            },
            decoration: InputDecoration(
              labelText: text,
              border: InputBorder.none,
              labelStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
  }

  Widget ffSummary(TextEditingController cont, String text) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 1),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 10, right: 10),
          child: TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: 3,
            maxLength: 100,
            controller: cont,
            onSaved: (value) {
              cont.text = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Data Kosong';
              }
              return null;
            },
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: text,
              labelStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
  }

  Widget chips(String text, Color warna, bool status) {
    return InkWell(
      onTap: () {
        setState(() {
          status = !status;
          print('status $text :: $status');
        });

        if (status == true) {
          if (!_selected.contains(text)) {
            print('tidak punya $text');
            _selected.add(text);
          }
        } else {
          _selected.remove(text);
        }
      },
      child: Container(
        child: Chip(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          labelPadding: EdgeInsets.all(5.0),
          label: Text(
            text,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: status != true ? warna : warna.withOpacity(0.9),
        ),
      ),
    );
  }
}
